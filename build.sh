#!/bin/bash
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o yikesbot .
docker build . -t ckevi/yikesbot:latest
docker tag ckevi/yikesbot:latest ckevi/yikesbot:1.7.5
docker push ckevi/yikesbot:latest
docker push ckevi/yikesbot:1.7.5

