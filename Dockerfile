FROM busybox
LABEL maintainer="CKEVI <admin@purpl3.net>"
RUN mkdir -p /opt/yikesbot/

COPY ca-certificates.crt /etc/ssl/certs/
COPY yikesbot /opt/yikesbot/yikesbot
COPY ./config.json /opt/yikesbot/config.json

USER nobody
EXPOSE 9998
ENTRYPOINT [ "/opt/yikesbot/yikesbot" ]

